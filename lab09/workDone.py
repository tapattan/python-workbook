import requests
import json 
import datetime 

#url ของ server ที่คุณมี
webhook_url = 'http://127.0.0.1:5000/webhook'

dt = str(datetime.datetime.now())
data = {'name':'george','event':'process complete','datetime':dt}

r = requests.post(webhook_url,data = json.dumps(data),headers={'Content-Type':'application/json'})