from calendar import c
from flask import Flask,request,abort 
from flask import render_template

app = Flask(__name__)

@app.route('/kPage',methods=['GET'])
def pagestart():
   return render_template("result2.html")

@app.route('/webhook',methods=['POST'])
def webhook():
    if(request.method == 'POST'): #ผู้บริการแจ้งว่าเสร็จแล้ว
        print(request.json)  
        file = open('read.txt', 'w') #เก็บข้อมูลลง log
        file.write(str(request.json))
        file.close() 
        return 'success',200
    else:
        abort(400)

if __name__ == '__main__':
    app.run(use_reloader=True,debug=True)