from fastapi import FastAPI
from fastapi.responses import HTMLResponse

import pandas as pd
import numpy as np
from tvDatafeed import TvDatafeed, Interval

app = FastAPI()

@app.get("/")
async def root():
    return {"message": "Hello World"}

@app.get("/stockprice/{symbol}", response_class=HTMLResponse)
async def read_items(symbol:str):
    
    tv = TvDatafeed()
    df = tv.get_hist(symbol=symbol,exchange='set',interval=Interval.in_daily,n_bars=100)
    df['Date'] = pd.to_datetime(df.index)
    df['Date'] = df['Date'].dt.strftime('%Y-%m-%d')
    df['Date'] = pd.to_datetime(df['Date'])
    df['no'] = np.array(range(1,len(df)+1))
    
    df = df.reset_index()
    df = df[['no','Date','open','high','low','close','volume']]  
    
    

    #https://medium.com/analytics-vidhya/5-css-snippets-for-better-looking-pandas-dataframes-ad808e407894

    stylecss = '''<style>.dataframe th{
background: rgb(61,164,166);
background: linear-gradient(0deg, rgba(61,164,166,1) 0%, rgba(31,198,190,1) 100%);
padding: 10px;
font-family: arial;
font-size: 120%;
color: white;
border:2px solid white;
text-align:left !important;}
</style>'''
    return stylecss+df.to_html(index=False) #